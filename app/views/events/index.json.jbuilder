json.array!(@events) do |event|
  json.extract! event, :id, :project, :description
  json.title event.project
  json.start event.start_time
  json.end event.end_time
  json.url event_url(event, format: :html)
end
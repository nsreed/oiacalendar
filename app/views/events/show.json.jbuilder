json.extract! @event, :id, :project, :description, :responsible_person, :gov_type, :due_date, :completed_date, :start_time, :end_time, :created_at, :updated_at

class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :project
      t.text :description
      t.string :responsible_person
      t.string :gov_type
      t.datetime :due_date
      t.datetime :completed_date
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
  end
end
